var express = require('express');
var bodyParser = require('body-parser');
var app = express();
const cors = require('cors');
const mysql = require('mysql');
var uuid = require('uuid');
const aws_keys = require('./creds_template');

var corsOptions = { origin: true, optionsSuccessStatus: 200 };
app.use(cors(corsOptions));
app.use(bodyParser.json({ limit: '10mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))
var port = 3000;

app.listen(port);
console.log('Listening on port', port);


//instanciamos el sdk
var AWS = require('aws-sdk');
//instanciamos los servicios a utilizar con sus respectivos accesos.
const s3 = new AWS.S3(aws_keys.s3);
//const ddb = new AWS.DynamoDB(aws_keys.dynamodb);
//const rek = new AWS.Rekognition(aws_keys.rekognition);
//const translate = new AWS.Translate(aws_keys.translate);
//const cognito = new AmazonCognitoIdentity.CognitoUserPool(aws_keys.cognito);



//--------------------------------------------------ALMACENAMIENTO---------------------------------------

//subir foto en s3
app.post('/subirArchivo', function (req, res) {

  /*
    imagen base 64
    extension
    direccion
    tipo (tipo)
  */
  var id = req.body.id; //main/hoja.jpg  main/caper/jo.jpg
  var foto = req.body.foto;
  console.log("id: " + id)
  //carpeta y nombre que quieran darle a la imagen

  var nombrei  = "fotos/" + id + ".jpg";
  nombrei = nombrei.toString();
  console.log(foto)
  //se convierte la base64 a bytes
  var b = new Buffer(foto.toString(), 'base64')
  const params = {
    Bucket: "selvinlisandro",
    Key: nombrei.toString(),
    Body: b,
    ContentType: "image",
    ACL: 'public-read'
  };
  try {
    const putResult = s3.putObject(params).promise();
  res.json({ mensaje: putResult })
  } catch (error) {
    console.err(error)
  }
  



});

//obtener foto en s3
app.post('/obtenerfoto', function (req, res) {
  var id = req.body.id;
  //direcccion donde esta el archivo a obtener
  var nombrei = "fotos/" + id + ".jpg";
  var getParams = {
    Bucket: 'selvinlisandro',
    Key: nombrei.toString()
  }
  s3.getObject(getParams, function (err, data) {
    if (err)
      res.json({ mensaje: "error" })
    //de bytes a base64
    var dataBase64 = Buffer.from(data.Body).toString('base64');
    res.json({ mensaje: dataBase64 })

  });

});
