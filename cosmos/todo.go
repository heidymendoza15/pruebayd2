package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"bytes"
	"time"
	_ "github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/dialers/mysql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	_ "github.com/ziutek/mymysql/godrv"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"github.com/shirou/gopsutil/cpu"
	//"github.com/shirou/gopsutil/mem"
)

// You will be using this Trainer type later in the program
type Trainer struct {
	Name string
	Age  int
	City string
}

type BASES1T struct {
	Nombre     string
	Comentario string
	Fecha      string
	Hashtags   []string
	Upvotes    int
	Downvotes  int
}

type BASES1 struct {
	Name         string
	Location     string
	Age          int
	Infectedtype string
	State        string
}
type Ram struct {
	Total      string `json:"total"`
	Libre1	   string `json:"libre"`
	Porcentaje string `json:"porcentaje"`
	Usado      string `json:"usado"`
}

type Cpu struct {
	Nucleo1 string `json:"nucleo1"`
	Libre1  string `json:"libre1"`
}

//FUNCIONES PRINCIPALES==========================
func main() {
	router := mux.NewRouter().StrictSlash(true)
	//COSMOS AZURE
	router.HandleFunc("/iniciarCargaCOSMO", Coneccion).Methods("GET")
	router.HandleFunc("/publicarCOSMO", createTask).Methods("POST")
	router.HandleFunc("/finalizarCargaCOSMO", CerrarConeccion).Methods("GET")
	router.HandleFunc("/verCOSMMO", getTasks).Methods("GET")
	//CLOUD SQL
	router.HandleFunc("/ConeccionCLOUD", ConeccionCLOUD).Methods("GET")
	router.HandleFunc("/publicarCLOUD", createTaskCLOUD).Methods("POST")
	router.HandleFunc("/finalizarCargaCLOUD", CerrarConeccionCLOUD).Methods("GET")

	//METDOS EN GENERAL
	router.HandleFunc("/iniciarCarga", INICIARCAARGAHEIDY).Methods("GET")
	router.HandleFunc("/publicar", createTaskGENERAL2).Methods("POST")
	router.HandleFunc("/finalizarCarga", FINALIZACARGAHEIDY).Methods("GET")
	//PARA VER LOS DATOS
	router.HandleFunc("/publicar", getTasks).Methods("GET")

	// MODULOS KERNEL
	router.HandleFunc("/Ram", dataRAM).Methods("GET")
	router.HandleFunc("/Cpu", dataCPU).Methods("GET")

	fmt.Println("ESPERANDO RUTA.....!")
	log.Fatal(http.ListenAndServe(":3000", router))
	//dbConn()
}
//MODULOS KERNEL

func dataRAM(w http.ResponseWriter, req *http.Request) {
	println("******** Cargar Datos CPU******")
	println("sopes")
	b, err := ioutil.ReadFile("/proc/ram-module") // obtenemos el archivo
	if err != nil {
		fmt.Print(err)
	}
	str := string(b) // convert content to a 'string'

	//fmt.Println(str) // print the content as a 'string'

	json.NewEncoder(w).Encode(str)
	fmt.Println(str)
}
func dataCPU(w http.ResponseWriter, r *http.Request) {
	println("******** Cargar Datos CPU******")
	println("sopes")
	b, err := ioutil.ReadFile("/proc/readproc-module") // obtenemos el archivo
	if err != nil {
		fmt.Print(err)
	}
	str := string(b) // convert content to a 'string'

	fmt.Println(str) // print the content as a 'string'

	percent, _ := cpu.Percent(time.Second, true)
	var nucleo1 = percent[0] 
	var libre1 = 100 - nucleo1 
	fmt.Printf("nucleo 1: %v\n", nucleo1) 
	fmt.Printf("libre 1: %v,\n", libre1)
	dato := Cpu{
		Nucleo1: fmt.Sprintf("%v", nucleo1),
		Libre1:  fmt.Sprintf("%v", libre1),
	}
	c, err := json.MarshalIndent(dato, "", "  ")
	if err != nil {
		fmt.Println(err)
	}
	// valores en consola
	fmt.Print(string(c))
	// enviamos en formato json los datos del cpu mediante peticion http
	json.NewEncoder(w).Encode(dato)
}

//METODOS DE LA BASE DE DATOS DE COSMOS AZURE

func Coneccion(w http.ResponseWriter, r *http.Request) {
	fmt.Println("ESTABLECIENDO CONECCION.....!")
	//CONECCION============================================
	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://cosmossopes:8zitaTuf4M22M5odtFgD3YsHMHiiMd8lWtEoQQD6vS6nF4W0lziDdmtc4KYUV9MAnS8NiUxRuG8fF7FC3w00MQ==@cosmossopes.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@cosmossopes@")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode("CONEXION CREADA")
}

func CerrarConeccion(w http.ResponseWriter, r *http.Request) {

	//CONECCION============================================
	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://cosmossopes:8zitaTuf4M22M5odtFgD3YsHMHiiMd8lWtEoQQD6vS6nF4W0lziDdmtc4KYUV9MAnS8NiUxRuG8fF7FC3w00MQ==@cosmossopes.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@cosmossopes@")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	//INSERT
	fmt.Println("Connected to MongoDB!")
	//DESCONECCION
	err = client.Disconnect(context.TODO())

	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connection to MongoDB closed.")
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode("CONEXION CERRADA")
}

func createTask(w http.ResponseWriter, r *http.Request) {
	//OBTENEMOS EL JSON
	newTask := make([]BASES1T, 1)
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Insert a Valid Task Data")
	}
	json.Unmarshal(reqBody, &newTask)

	//ESTABLECEMOS CONECCION
	clientOptions := options.Client().ApplyURI("mongodb://cosmossopes:8zitaTuf4M22M5odtFgD3YsHMHiiMd8lWtEoQQD6vS6nF4W0lziDdmtc4KYUV9MAnS8NiUxRuG8fF7FC3w00MQ==@cosmossopes.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@cosmossopes@")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connected to MongoDB!")

	//INSERTAMOS CON LA CONECCION RECORRIENDO JSON
	for _, te := range newTask {
		fmt.Println("--------")
		fmt.Println(te)
		//===============================
		collection := client.Database("BASESOPES1").Collection("TABLAUNICA")
		ash := BASES1T{te.Nombre, te.Comentario, te.Fecha, te.Hashtags, te.Upvotes, te.Downvotes}
		trainers := []interface{}{ash}
		insertManyResult, err := collection.InsertMany(context.TODO(), trainers)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("DATO INSERTADO CON EXITO... ", insertManyResult.InsertedIDs)
		//===============================
	}

	//RETORNO EXITOSO
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newTask)
}

func getTasks(w http.ResponseWriter, r *http.Request) {

	fmt.Println("VER DATOS.....!")
	//CONEXION
	clientOptions := options.Client().ApplyURI("mongodb://cosmossopes:8zitaTuf4M22M5odtFgD3YsHMHiiMd8lWtEoQQD6vS6nF4W0lziDdmtc4KYUV9MAnS8NiUxRuG8fF7FC3w00MQ==@cosmossopes.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@cosmossopes@")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connected to MongoDB!")
	collection := client.Database("BASESOPES1").Collection("TABLAUNICA")

	//VER TODOS
	var results []*BASES1T
	// Passing bson.D{{}} as the filter matches all documents in the collection
	cur, err := collection.Find(context.TODO(), bson.D{{}})
	if err != nil {
		log.Fatal(err)
	}
	// Finding multiple documents returns a cursor
	// Iterating through the cursor allows us to decode documents one at a time
	for cur.Next(context.TODO()) {
		// create a value into which the single document can be decoded
		var elem BASES1T
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}

		results = append(results, &elem)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	// Close the cursor once finished
	cur.Close(context.TODO())

	fmt.Printf("Found multiple documents (array of pointers): %+v\n", results)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(results)

}

//METODOS DE LA BASE DE DATOS DE CLOUD SQL
func ConeccionCLOUD(w http.ResponseWriter, r *http.Request) {
	fmt.Println("ESTABLECIENDO CONECCION.....!")
	db, err := sql.Open("mysql", "tem@cloudsql(so1proyecto1-324804:us-central1:p1db)/temporal")

	if err != nil {
		fmt.Println(err)
		log.Fatalf("Failed conhhnection")
	}
	fmt.Println("CONECTADO......")

	defer db.Close()

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode("CONEXION CREADA")
}

func createTaskCLOUD(w http.ResponseWriter, r *http.Request) {
	//OBTENEMOS EL JSON
	newTask := make([]BASES1T, 1)
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Insert a Valid Task Data")
	}
	json.Unmarshal(reqBody, &newTask)

	//ESTABLECEMOS CONECCION
	db, err := sql.Open("mysql", "tem@cloudsql(so1proyecto1-324804:us-central1:p1db)/BASESOPES1")

	if err != nil {
		fmt.Println(err)
		log.Fatalf("Failed conhhnection")
	}
	fmt.Println("CONECTADO......")

	//INSERTAMOS CON LA CONECCION RECORRIENDO JSON
	for _, te := range newTask {
		fmt.Println("--------")
		fmt.Println(te)
		//===============================
		nombre := te.Nombre
		comentario := te.Comentario
		fecha := te.Fecha
		hashtagsT := te.Hashtags
		upvotes := te.Upvotes
		downvotes := te.Downvotes

		fmt.Println("===ARREGLO")
		stringArray := hashtagsT
		hashtags := strings.Join(stringArray, ",")
		fmt.Println(hashtags)
		fmt.Println("===FF")

		insForm, err := db.Prepare("INSERT INTO TABLAUNICA(nombre,comentario,fecha,hashtags,upvotes,downvotes) VALUES(?,?,?,?,?,?)")
		if err != nil {
			panic(err.Error())
		}

		insForm.Exec(nombre, comentario, fecha, hashtags, upvotes, downvotes)
		log.Println("INSERT: Name: " + nombre + " | Location: " + comentario)
		//===============================
	}

	//RETORNO EXITOSO
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newTask)
}

func CerrarConeccionCLOUD(w http.ResponseWriter, r *http.Request) {
	fmt.Println("ESTABLECIENDO CONECCION.....!")
	db, err := sql.Open("mysql", "tem@cloudsql(so1proyecto1-324804:us-central1:p1db)/temporal")

	if err != nil {
		fmt.Println(err)
		log.Fatalf("Failed conhhnection")
	}
	fmt.Println("CONECTADO......")

	defer db.Close()

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode("CONEXION CERRADA")
}

//==============================================================TEMPORALES==========================================================
//FUNCIONES TEMPORALES COSMOS=====================================

func indexRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Wecome the my GO API!")
}

func createTask2(w http.ResponseWriter, r *http.Request) {
	var newTask Trainer
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Insert a Valid Task Data")
	}

	json.Unmarshal(reqBody, &newTask)

	//newTask.ID = len(tasks) + 1
	//tasks = append(tasks, newTask)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newTask)
}

func Insertar() {
	fmt.Println("INSERTANDO DATOS.....!")
	//CONECCION============================================
	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://cosmossopes:8zitaTuf4M22M5odtFgD3YsHMHiiMd8lWtEoQQD6vS6nF4W0lziDdmtc4KYUV9MAnS8NiUxRuG8fF7FC3w00MQ==@cosmossopes.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@cosmossopes@")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	//INSERT
	fmt.Println("Connected to MongoDB!")
	collection := client.Database("test").Collection("trainers")
	ash := Trainer{"Ash", 10, "Pallet Town"}
	misty := Trainer{"Misty", 10, "Cerulean City"}
	brock := Trainer{"Brock", 15, "Pewter City"}

	trainers := []interface{}{ash, misty, brock}

	insertManyResult, err := collection.InsertMany(context.TODO(), trainers)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Inserted multiple documents: ", insertManyResult.InsertedIDs)
}

func VerDatos() {
	fmt.Println("VER DATOS.....!")
	//CONECCION============================================
	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://cosmossopes:8zitaTuf4M22M5odtFgD3YsHMHiiMd8lWtEoQQD6vS6nF4W0lziDdmtc4KYUV9MAnS8NiUxRuG8fF7FC3w00MQ==@cosmossopes.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@cosmossopes@")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	//INSERT
	fmt.Println("Connected to MongoDB!")
	collection := client.Database("test").Collection("trainers")
	//VER TODOS
	// Here's an array in which you can store the decoded documents
	var results []*Trainer

	// Passing bson.D{{}} as the filter matches all documents in the collection
	cur, err := collection.Find(context.TODO(), bson.D{{}})
	if err != nil {
		log.Fatal(err)
	}
	// Finding multiple documents returns a cursor
	// Iterating through the cursor allows us to decode documents one at a time
	for cur.Next(context.TODO()) {

		// create a value into which the single document can be decoded
		var elem Trainer
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}

		results = append(results, &elem)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	// Close the cursor once finished
	cur.Close(context.TODO())

	fmt.Printf("Found multiple documents (array of pointers): %+v\n", results)
}

func EMERGECICA() {
	//CONECCION============================================
	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://cosmossopes:8zitaTuf4M22M5odtFgD3YsHMHiiMd8lWtEoQQD6vS6nF4W0lziDdmtc4KYUV9MAnS8NiUxRuG8fF7FC3w00MQ==@cosmossopes.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@cosmossopes@")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	//INSERT
	fmt.Println("Connected to MongoDB!")
	collection := client.Database("test").Collection("trainers")
	ash := Trainer{"Ash", 10, "Pallet Town"}
	misty := Trainer{"Misty", 10, "Cerulean City"}
	brock := Trainer{"Brock", 15, "Pewter City"}

	trainers := []interface{}{ash, misty, brock}

	insertManyResult, err := collection.InsertMany(context.TODO(), trainers)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Inserted multiple documents: ", insertManyResult.InsertedIDs)

	//VER TODOS

	// Here's an array in which you can store the decoded documents
	var results []*Trainer

	// Passing bson.D{{}} as the filter matches all documents in the collection
	cur, err := collection.Find(context.TODO(), bson.D{{}})
	if err != nil {
		log.Fatal(err)
	}
	// Finding multiple documents returns a cursor
	// Iterating through the cursor allows us to decode documents one at a time
	for cur.Next(context.TODO()) {

		// create a value into which the single document can be decoded
		var elem Trainer
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}

		results = append(results, &elem)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	// Close the cursor once finished
	cur.Close(context.TODO())

	fmt.Printf("Found multiple documents (array of pointers): %+v\n", results)

	//DESCONECCION
	err = client.Disconnect(context.TODO())

	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connection to MongoDB closed.")
}

//FUNCIONES TEMPORALES CLOUD SQL=====================================

func dbConn() {
	//dbDriver := "mysql"
	//dbUser := "root"
	//dbPass := "root"
	//dbInstance := "so1proyecto1-324804:us-central1:p1db"
	//dbName := "temporal"
	db, err := sql.Open("mysql", "tem@cloudsql(so1proyecto1-324804:us-central1:p1db)/temporal")
	//db, err := sql.Open("mymysql", "cloudsql:so1proyecto1-324804:us-central1:p1db*temporal/admin/admin")
	//db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@unix(/cloudsql/"+dbInstance+")/"+dbName)
	if err != nil {
		fmt.Println(err)
		log.Fatalf("Failed conhhnection")
	}
	fmt.Println("CONECTADO......")

	email := "nuevocoorreww332"
	name := "nuevonombrew2w33"

	insForm, err := db.Prepare("INSERT INTO tableName(email_address, name) VALUES(?,?)")
	if err != nil {
		panic(err.Error())
	}

	insForm.Exec(email, name)
	log.Println("INSERT: Email: " + email + " | Name: " + name)
	defer db.Close()
}

//==================================================================
//METODOS GENERALES
func ConeccionGeneral(w http.ResponseWriter, r *http.Request) {
	fmt.Println("ESTABLECIENDO CONECCION AZURE.....!")
	//CONECCION============================================
	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://cosmossopes:8zitaTuf4M22M5odtFgD3YsHMHiiMd8lWtEoQQD6vS6nF4W0lziDdmtc4KYUV9MAnS8NiUxRuG8fF7FC3w00MQ==@cosmossopes.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@cosmossopes@")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode("CONEXION CREADA AZURE")

	fmt.Println("ESTABLECIENDO CONECCION CLOUD.....!")
	db, err := sql.Open("mysql", "tem@cloudsql(so1proyecto1-324804:us-central1:p1db)/temporal")

	if err != nil {
		fmt.Println(err)
		log.Fatalf("Failed conhhnection")
	}
	fmt.Println("CONECTADO......")

	defer db.Close()

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode("CONEXION CREADA CLOUD")
}

func CerrarConeccionGeneral(w http.ResponseWriter, r *http.Request) {

	//CONECCION============================================
	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://cosmossopes:8zitaTuf4M22M5odtFgD3YsHMHiiMd8lWtEoQQD6vS6nF4W0lziDdmtc4KYUV9MAnS8NiUxRuG8fF7FC3w00MQ==@cosmossopes.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@cosmossopes@")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	//INSERT
	fmt.Println("Connected to MongoDB! AZURE")
	//DESCONECCION
	err = client.Disconnect(context.TODO())

	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connection to MongoDB closed.")
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode("CONEXION CERRADA AZURE")

	fmt.Println("ESTABLECIENDO CONECCION CLOUD.....!")
	db, err := sql.Open("mysql", "tem@cloudsql(so1proyecto1-324804:us-central1:p1db)/temporal")

	if err != nil {
		fmt.Println(err)
		log.Fatalf("Failed conhhnection")
	}
	fmt.Println("CONECTADO......")

	defer db.Close()

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode("CONEXION CERRADA CLOUD")
}

//CON CORECHETES
func createTaskGENERAL(w http.ResponseWriter, r *http.Request) {
	//OBTENEMOS EL JSON
	newTask := make([]BASES1T, 1)
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Insert a Valid Task Data AZURE")
	}
	json.Unmarshal(reqBody, &newTask)

	//ESTABLECEMOS CONECCION
	clientOptions := options.Client().ApplyURI("mongodb://cosmossopes:8zitaTuf4M22M5odtFgD3YsHMHiiMd8lWtEoQQD6vS6nF4W0lziDdmtc4KYUV9MAnS8NiUxRuG8fF7FC3w00MQ==@cosmossopes.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@cosmossopes@")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connected to MongoDB!")

	//INSERTAMOS CON LA CONECCION RECORRIENDO JSON
	for _, te := range newTask {
		fmt.Println("--------")
		fmt.Println(te)
		//===============================
		collection := client.Database("BASESOPES1").Collection("TABLAUNICA")
		ash := BASES1T{te.Nombre, te.Comentario, te.Fecha, te.Hashtags, te.Upvotes, te.Downvotes}
		trainers := []interface{}{ash}
		insertManyResult, err := collection.InsertMany(context.TODO(), trainers)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("DATO INSERTADO CON EXITO AZURE... ", insertManyResult.InsertedIDs)
		//===============================
	}

	//RETORNO EXITOSO
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newTask)

	//OBTENEMOS EL JSON
	if err != nil {
		fmt.Fprintf(w, "Insert a Valid Task Data")
	}
	json.Unmarshal(reqBody, &newTask)

	//ESTABLECEMOS CONECCION
	db, err := sql.Open("mysql", "tem@cloudsql(so1proyecto1-324804:us-central1:p1db)/BASESOPES1")

	if err != nil {
		fmt.Println(err)
		log.Fatalf("Failed conhhnection")
	}
	fmt.Println("CONECTADO......")

	//INSERTAMOS CON LA CONECCION RECORRIENDO JSON
	for _, te := range newTask {
		fmt.Println("--------")
		fmt.Println(te)
		//===============================
		nombre := te.Nombre
		comentario := te.Comentario
		fecha := te.Fecha
		hashtagsT := te.Hashtags
		upvotes := te.Upvotes
		downvotes := te.Downvotes

		fmt.Println("===ARREGLO")
		stringArray := hashtagsT
		hashtags := strings.Join(stringArray, ",")
		fmt.Println(hashtags)
		fmt.Println("===FF")

		insForm, err := db.Prepare("INSERT INTO TABLAUNICA(nombre,comentario,fecha,hashtags,upvotes,downvotes) VALUES(?,?,?,?,?,?)")
		if err != nil {
			panic(err.Error())
		}

		insForm.Exec(nombre, comentario, fecha, hashtags, upvotes, downvotes)
		log.Println("INSERT: Name: " + nombre + " | Location: " + comentario)
		//===============================
	}

	//RETORNO EXITOSO
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newTask)

}

//SIN CORCHETES
func createTaskGENERAL2(w http.ResponseWriter, r *http.Request) {
	//OBTENEMOS EL JSON
	var newTask BASES1T
	//newTask := make([]BASES1T, 1)
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Insert a Valid Task Data AZURE")
	}
	json.Unmarshal(reqBody, &newTask)

	//ESTABLECEMOS CONECCION
	clientOptions := options.Client().ApplyURI("mongodb://cosmossopes:8zitaTuf4M22M5odtFgD3YsHMHiiMd8lWtEoQQD6vS6nF4W0lziDdmtc4KYUV9MAnS8NiUxRuG8fF7FC3w00MQ==@cosmossopes.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@cosmossopes@")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connected to MongoDB!")

	//INSERTAMOS CON LA CONECCION RECORRIENDO JSON
	//for _, te := range newTask {
	fmt.Println("--------")
	fmt.Println(newTask)
	//===============================
	collection := client.Database("BASESOPES1").Collection("TABLAUNICA")
	ash := BASES1T{newTask.Nombre, newTask.Comentario, newTask.Fecha, newTask.Hashtags, newTask.Upvotes, newTask.Downvotes}
	//trainers := []interface{}{ash}
	insertManyResult, err := collection.InsertOne(context.TODO(), ash)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("DATO INSERTADO CON EXITO AZURE... ", insertManyResult.InsertedID)
	//===============================
	//}

	//RETORNO EXITOSO
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newTask)

	//OBTENEMOS EL JSON
	if err != nil {
		fmt.Fprintf(w, "Insert a Valid Task Data")
	}
	json.Unmarshal(reqBody, &newTask)

	//ESTABLECEMOS CONECCION
	db, err := sql.Open("mysql", "tem@cloudsql(so1proyecto1-324804:us-central1:p1db)/BASESOPES1")

	if err != nil {
		fmt.Println(err)
		log.Fatalf("Failed conhhnection")
	}
	fmt.Println("CONECTADO......")

	//INSERTAMOS CON LA CONECCION RECORRIENDO JSON
	//for _, te := range newTask {
	fmt.Println("--------")
	fmt.Println(newTask)
	//===============================
	nombre := newTask.Nombre
	comentario := newTask.Comentario
	fecha := newTask.Fecha
	hashtagsT := newTask.Hashtags
	upvotes := newTask.Upvotes
	downvotes := newTask.Downvotes

	fmt.Println("===ARREGLO")
	stringArray := hashtagsT
	hashtags := strings.Join(stringArray, ",")
	fmt.Println(hashtags)
	fmt.Println("===FF")

	insForm, err := db.Prepare("INSERT INTO TABLAUNICA(nombre,comentario,fecha,hashtags,upvotes,downvotes) VALUES(?,?,?,?,?,?)")
	if err != nil {
		panic(err.Error())
	}

	insForm.Exec(nombre, comentario, fecha, hashtags, upvotes, downvotes)
	log.Println("INSERT: Name: " + nombre + " | Location: " + comentario)
	//===============================
	//}

	//POST===========

	postBody, _ := json.Marshal(map[string]string{
		"api": "go",
	})
	responseBody := bytes.NewBuffer(postBody)
	//Leverage Go's HTTP Post function to make request
	resp, err := http.Post("http://34.122.236.88:5001/publicar", "application/json", responseBody)
	//Handle Error
	if err != nil {
		log.Fatalf("An Error Occured %v", err)
	}
	defer resp.Body.Close()
	//Read the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	sb := string(body)
	log.Printf(sb)

	//TERM

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newTask)

}

//PUBSUB=================

func INICIARCAARGAHEIDY(w http.ResponseWriter, r *http.Request) {
	fmt.Println("ESTABLECIENDO CONECCION.....!")
	//CONECCION============================================
	// Set client options
	resp, err := http.Get("http://34.122.236.88:5001/iniciaCarga")
	if err != nil {
		log.Fatalln(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	sb := string(body)
	log.Printf(sb)

}

func FINALIZACARGAHEIDY(w http.ResponseWriter, r *http.Request) {
	fmt.Println("ESTABLECIENDO CONECCION.....!")
	//CONECCION============================================
	// Set client options
	resp, err := http.Get("http://34.122.236.88:5001/finCarga")
	if err != nil {
		log.Fatalln(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	sb := string(body)
	log.Printf(sb)

}
