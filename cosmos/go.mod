module github.com/abhirockzz/monogdb-go-quickstart

go 1.14

require (
	github.com/GoogleCloudPlatform/cloudsql-proxy v1.25.0 // indirect
	github.com/StackExchange/wmi v1.2.1 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/kisielk/errcheck v1.2.0 // indirect
	github.com/olekukonko/tablewriter v0.0.4
	github.com/shirou/gopsutil v3.21.9+incompatible // indirect
	github.com/tklauser/go-sysconf v0.3.9 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v0.0.0-20180714160509-73f8eece6fdc // indirect
	github.com/ziutek/mymysql v1.5.4 // indirect
	go.mongodb.org/mongo-driver v1.7.2
)
